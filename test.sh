#!/bin/sh
#
# A test script that will start a number of nodes using cmd/node.go
#

n=${1:-20}
base_port=2000

go build cmd/node.go
go build test/probe.go

./node --addr=127.0.0.1:${base_port} --name=node-0 &
all_nodes="127.0.0.1:${base_port}"
for i in $(seq 1 $(( $n - 1 ))) ; do
    port=$(( $i + $base_port ))
    name="node-$i"
    all_nodes="${all_nodes} 127.0.0.1:${port}"
    ./node --addr=127.0.0.1:${port} --name=${name} 127.0.0.1:${base_port} &
done

./probe ${all_nodes} &

echo "press key when done."

read ans

pkill node probe

