package gossip

import (
	"errors"
	"fmt"
	"math/rand"
	"os"
	"reflect"
	"strings"
	"sync"
	"testing"
	"time"

	"git.autistici.org/ale/rrpc"
	"github.com/joliv/spark"
)

var (
	tick = 20 * time.Millisecond

	someTime = 10 * time.Second
)

func TestGossip_ReceiveDoesNotAddSelf(t *testing.T) {
	g := New("node1", "node1", nil)
	g.DebugOutput = true
	if len(g.members) != 1 {
		t.Fatalf("len(g.members) is %d, want 1", len(g.members))
	}
	if g.members["node1"] == nil {
		t.Fatalf("self not added to g.members")
	}

	g.Receive([]Member{{"node1", 1, "node1"}})
	if len(g.members) != 1 {
		t.Fatalf("post-Receive len(g.members) is %d, want 1", len(g.members))
	}

	members := g.GetMembers()
	if len(members) != 1 {
		t.Fatalf("GetMembers() expected 1 result: %+v", members)
	}
}

func TestGossip_ReceiveAddsNode(t *testing.T) {
	g := New("node1", "node1", nil)
	g.DebugOutput = true
	if len(g.members) != 1 {
		t.Fatalf("len(g.members) is %d, want 1", len(g.members))
	}
	if g.members["node1"] == nil {
		t.Fatalf("self not added to g.members")
	}

	// Test adding the same node multiple times.
	for i := 0; i < 2; i++ {
		g.Receive([]Member{{"node1", 1, "node1"}, {"node2", 1, "node2"}})
		if len(g.members) != 2 {
			t.Fatalf("%d: len(g.members) is %d, want 2", i, len(g.members))
		}
		if g.members["node2"] == nil {
			t.Fatalf("%d: node2 not added to g.members", i)
		}

		members := g.GetMembers()
		if len(members) != 2 {
			t.Fatalf("%d: GetMembers() expected 2 results: %+v", i, members)
		}
	}

	// Verify integrity of g.members.
	for addr, m := range g.members {
		if addr != m.Addr {
			t.Errorf("inconsistent addr in g.members: addr=%s, m=%+v", addr, m)
		}
	}
}

func memberListToMap(ml []Member) map[string]struct{} {
	out := make(map[string]struct{})
	for _, m := range ml {
		out[m.Addr] = struct{}{}
	}
	return out
}

func memberListEqual(a, b []Member) bool {
	return len(a) == len(b) && reflect.DeepEqual(memberListToMap(a), memberListToMap(b))
}

// Test cluster simulator.
type testCluster struct {
	nodes    map[string]*Gossip
	faults   map[string]bool
	quit     chan bool
	lock     sync.Mutex
	maxDelay int
	delays   []float64
}

// An RPC interface that is bound to a specific source.
type testClusterRpcInterface struct {
	cluster *testCluster
	src     string
}

func (c *testClusterRpcInterface) Call(addr, method string, req, resp interface{}, opts *rrpc.Options) error {
	return c.cluster.Call(c.src, addr, method, req, resp, opts)
}

func (r *testCluster) Call(src, dst, method string, req, resp interface{}, opts *rrpc.Options) error {
	r.lock.Lock()
	defer r.lock.Unlock()

	// Who needs introspection when we have a single method
	g, ok := r.nodes[dst]
	if !ok {
		return errors.New("node not found")
	}

	// Faults are bidirectional.
	for _, addr := range []string{src, dst} {
		if _, ok := r.faults[addr]; ok {
			return errors.New("network error")
		}
	}

	if method == "Gossip.Receive" {
		// To properly simulate an RPC we need to make a deep
		// copy of the request, the local method may mutate
		// the data.
		gossipReq := req.(*GossipRequest)
		g.Receive(gossipReq.Members)
		return nil
	}
	return errors.New("unknown method")
}

func (r *testCluster) New(addr string, data interface{}, peers []string) *Gossip {
	g := New(addr, data, peers)
	g.rpc = &testClusterRpcInterface{r, addr}
	g.Tgossip = tick
	g.DebugOutput = (os.Getenv("GOSSIP_DEBUG") != "")

	r.lock.Lock()
	r.nodes[addr] = g
	r.lock.Unlock()

	return g
}

func (r *testCluster) AddFault(node string) {
	r.lock.Lock()
	defer r.lock.Unlock()

	r.faults[node] = true
}

func (r *testCluster) RemoveFault(node string) {
	r.lock.Lock()
	defer r.lock.Unlock()

	delete(r.faults, node)
}

// Measure data propagation delay across the non-faulty nodes of the
// cluster.
func (r *testCluster) measureDelay() int {
	curHeartbeat := time.Now().UnixNano() / 1000000
	var maxDelay int
	for n, g := range r.nodes {
		if _, ok := r.faults[n]; ok {
			continue
		}
		for _, m := range g.GetMembers() {
			if _, ok := r.faults[m.Addr]; ok {
				continue
			}
			if m.Heartbeat == 0 {
				continue
			}
			delay := int(curHeartbeat - m.Heartbeat)
			if delay > maxDelay {
				maxDelay = delay
			}
		}
	}
	return maxDelay
}

func (r *testCluster) Start() {
	r.lock.Lock()
	defer r.lock.Unlock()

	for _, g := range r.nodes {
		go g.Run()
	}

	// Run delay measurements in a goroutine.
	go func() {
		t := time.NewTicker(100 * time.Millisecond)
		for {
			select {
			case <-t.C:
				r.lock.Lock()
				delay := r.measureDelay()
				if delay > r.maxDelay {
					r.maxDelay = delay
				}
				r.delays = append(r.delays, float64(delay))
				r.lock.Unlock()
			case <-r.quit:
				return
			}
		}
	}()
}

func (r *testCluster) Stop() {
	r.lock.Lock()
	defer r.lock.Unlock()

	for _, g := range r.nodes {
		g.Close()
	}
	close(r.quit)
}

func (r *testCluster) RestartNode(name string) {
	r.lock.Lock()
	r.nodes[name].Close()
	r.lock.Unlock()

	g := r.New(name, name, []string{})
	go g.Run()
}

// Condition generator for full cluster agreement.
func (r *testCluster) allNodesSeeEachOther() func() bool {
	r.lock.Lock()
	defer r.lock.Unlock()

	var nodes []string
	for n := range r.nodes {
		nodes = append(nodes, n)
	}
	return r.partialAgreement(nodes)
}

// Condition generator for agreement between a subset of the cluster nodes.
func (r *testCluster) partialAgreement(nodes []string) func() bool {
	return func() bool {
		r.lock.Lock()
		defer r.lock.Unlock()

		var all [][]Member
		for _, n := range nodes {
			g := r.nodes[n]
			all = append(all, g.GetMembers())
		}
		ref := all[0]
		for i := 1; i < len(all); i++ {
			if !memberListEqual(ref, all[i]) {
				return false
			}
		}
		return true
	}
}

func (r *testCluster) dump(t *testing.T) {
	for n, g := range r.nodes {
		t.Logf("%s: members=%v", n, g.GetMembers())
	}
}

// Wait for a while for a condition to be true (and check it stays
// true for a little while).
func (r *testCluster) waitAtMost(t *testing.T, howlong time.Duration, cond func() bool, condDescr string) {
	deadline := time.Now().Add(howlong)
	count := 0
	for time.Now().Before(deadline) {
		if cond() {
			count++
			if count == 3 {
				t.Logf("cluster condition reached: %s", condDescr)
				return
			}
		} else {
			count = 0
		}
		time.Sleep(100 * time.Millisecond)
	}
	r.dump(t)
	t.Fatalf("timeout expired waiting for cluster condition: %s", condDescr)
}

// Wait for certain conditions.
func (r *testCluster) WaitForBootstrap(t *testing.T) {
	r.waitAtMost(t, someTime, r.allNodesSeeEachOther(), "bootstrap")
}

func (r *testCluster) WaitForFullAgreement(t *testing.T) {
	r.waitAtMost(t, someTime, r.allNodesSeeEachOther(), "full agreement")
}

func (r *testCluster) WaitForPartialAgreement(t *testing.T, nodes ...string) {
	r.waitAtMost(t, someTime, r.partialAgreement(nodes), fmt.Sprintf("partial agreement between %s", strings.Join(nodes, ", ")))
}

func (r *testCluster) assertThat(t *testing.T, what func() bool, descr string) {
	if !what() {
		r.dump(t)
		t.Fatalf("Failed %s", descr)
	}
}

// Assert that certain conditions are true.
func (r *testCluster) AssertPartialAgreement(t *testing.T, nodes ...string) {
	r.assertThat(t, r.partialAgreement(nodes), fmt.Sprintf("partial agreement between %s", strings.Join(nodes, ", ")))
}

func (r *testCluster) AssertFullAgreement(t *testing.T) {
	r.assertThat(t, r.allNodesSeeEachOther(), "full cluster agreement")
}

// Assert that the maximum data propagation delay (timestamp -
// heartbeat) measured across the active nodes in the cluster for the
// duration of the experiment is below some "reasonable" value which
// depends on the cluster size and Tgossip. Note that the propagation
// delay is unrelated to node heartbeats and failure time.
func (r *testCluster) AssertReasonableDelay(t *testing.T) {
	// Heartbeats are measured in milliseconds.
	maxDelay := time.Duration(r.maxDelay) * time.Millisecond
	// Give the expected max delay a fuzz factor of 3 (the
	// "reasonable" part :) -- by coincidence, this happens to be
	// the same formula used for FailureTime.
	maxExpectedDelay := time.Duration(3*len(r.nodes)) * tick

	t.Logf("cluster delay: %s", spark.Line(r.delays))

	if maxDelay < maxExpectedDelay {
		t.Logf("cluster max delay=%f, theoretical=%f", maxDelay.Seconds(), maxExpectedDelay.Seconds())
	} else {
		t.Errorf("cluster max delay out of range: got=%f, max_expected=%f", maxDelay.Seconds(), maxExpectedDelay.Seconds())
	}
}

func newTestCluster(names []string) *testCluster {
	c := &testCluster{
		nodes:  make(map[string]*Gossip),
		faults: make(map[string]bool),
		quit:   make(chan bool),
	}
	var bootstrap []string
	for _, name := range names {
		c.New(name, name, bootstrap)
		if bootstrap == nil {
			bootstrap = []string{name}
		}
	}
	c.Start()
	return c
}

// Test a simple two-node network.
func TestGossip_TwoNodeNetwork(t *testing.T) {
	c := newTestCluster([]string{"node1", "node2"})
	defer c.Stop()

	c.WaitForBootstrap(t)
	c.AssertFullAgreement(t)
}

// Three-node network test.
func TestGossip_ThreeNodeNetwork(t *testing.T) {
	c := newTestCluster([]string{"node1", "node2", "node3"})
	defer c.Stop()

	c.WaitForBootstrap(t)
	c.AssertFullAgreement(t)
	c.AssertReasonableDelay(t)
}

// Three-node network, should fall back to stable two-node config
// after a node fails.
func TestGossip_ThreeNodeNetworkWithFault(t *testing.T) {
	c := newTestCluster([]string{"node1", "node2", "node3"})
	defer c.Stop()

	c.WaitForBootstrap(t)
	t.Log("injecting failure on node1")
	c.AddFault("node1")
	c.WaitForPartialAgreement(t, "node2", "node3")

	// See if it sticks (though Wait already verifier this).
	time.Sleep(10 * tick)
	c.AssertPartialAgreement(t, "node2", "node3")
	c.AssertReasonableDelay(t)
}

// Four-node network with two node failures.
func TestGossip_FourNodeNetworkWithDoubleFault(t *testing.T) {
	c := newTestCluster([]string{"node1", "node2", "node3", "node4"})
	defer c.Stop()

	c.WaitForBootstrap(t)
	t.Log("injecting failure on node1")
	c.AddFault("node1")
	t.Log("injecting failure on node2")
	c.AddFault("node2")
	c.WaitForPartialAgreement(t, "node3", "node4")
	c.AssertReasonableDelay(t)
}

// Three-node network, with a failure that subsequently recovers. The
// cluster should revert to the original state.
func TestGossip_ThreeNodeNetworkWithFaultAndRecovery(t *testing.T) {
	c := newTestCluster([]string{"node1", "node2", "node3"})
	defer c.Stop()

	c.WaitForBootstrap(t)
	t.Log("injecting failure on node1")
	c.AddFault("node1")
	c.WaitForPartialAgreement(t, "node2", "node3")
	time.Sleep(10 * tick)
	t.Log("removing failure on node1")
	c.RemoveFault("node1")
	c.WaitForFullAgreement(t)
	c.AssertReasonableDelay(t)
}

// Large network, 50% node failure and recovery.
func TestGossip_LargeNetworkWithFaultAndRecovery(t *testing.T) {
	nn := 50
	var nodes []string
	for i := 0; i < nn; i++ {
		nodes = append(nodes, fmt.Sprintf("node%d", i+1))
	}
	goodNodes := nodes[0 : nn/2]
	badNodes := nodes[nn/2 : nn]

	c := newTestCluster(nodes)
	defer c.Stop()

	c.WaitForBootstrap(t)
	t.Log("injecting failure on 50% of nodes")
	for _, n := range badNodes {
		c.AddFault(n)
	}
	c.WaitForPartialAgreement(t, goodNodes...)
	time.Sleep(20 * tick)
	t.Log("removing failure")
	for _, n := range badNodes {
		c.RemoveFault(n)
	}
	c.WaitForFullAgreement(t)

	// Check stability of the cluster.
	for i := 0; i < 5; i++ {
		time.Sleep(2 * tick)
		c.AssertFullAgreement(t)
	}
	c.AssertReasonableDelay(t)
}

// Large network, really bad connectivity.
func TestGossip_LargeNetworkWithRandomFaults(t *testing.T) {
	nn := 50
	var nodes []string
	for i := 0; i < nn; i++ {
		nodes = append(nodes, fmt.Sprintf("node%d", i+1))
	}
	var goodNodes, badNodes []string

	randomPartition := func() ([]string, []string) {
		var good, bad []string
		for _, n := range nodes {
			if rand.Intn(2) == 0 {
				good = append(good, n)
			} else {
				bad = append(bad, n)
			}
		}
		return good, bad
	}

	c := newTestCluster(nodes)
	defer c.Stop()

	// Disturb the network by periodically cutting off 50% of the
	// nodes at a relatively high rate.
	t.Log("injecting random temporary failures")
	for i := 0; i < 20; i++ {
		if badNodes != nil {
			for _, n := range badNodes {
				c.RemoveFault(n)
			}
		}
		goodNodes, badNodes = randomPartition()
		for _, n := range badNodes {
			c.AddFault(n)
		}
		time.Sleep(5 * tick)
	}

	// The surviving part of the cluster should reach an agreement.
	c.WaitForPartialAgreement(t, goodNodes...)
	c.AssertReasonableDelay(t)
}

// Test node restart (which should be seamless).
func TestGossip_NodeRestart(t *testing.T) {
	c := newTestCluster([]string{"node1", "node2", "node3"})
	defer c.Stop()

	c.WaitForBootstrap(t)
	t.Log("restarting node1")
	c.RestartNode("node1")
	c.WaitForFullAgreement(t)
}

// Test the propagation time of a failure in a large network.
func TestGossip_FailurePropagation(t *testing.T) {
	var nodes []string
	for i := 0; i < 10; i++ {
		nodes = append(nodes, fmt.Sprintf("node%d", i+1))
	}
	c := newTestCluster(nodes)
	defer c.Stop()

	c.WaitForBootstrap(t)
	t.Log("injecting failure on node1")
	c.AddFault("node1")
	start := time.Now()
	remaining := nodes[1:]
	c.WaitForPartialAgreement(t, remaining...)
	elapsed := time.Since(start)

	maxFailureTime := 2 * c.nodes["node2"].FailureTime()
	if elapsed > maxFailureTime {
		t.Errorf("failure propagation time too long: %f > %f", elapsed, maxFailureTime)
	} else {
		t.Logf("failure propagation time=%f, max=%f", elapsed, maxFailureTime)
	}
}
