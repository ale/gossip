
gossip - a simple service discovery protocol
============================================

``gossip`` is a service discovery protocol that can broadcast
application-specific data across a number of participant nodes.

The underlying protocol is very simple. It is based on random
gossiping between participants, and it provides a few useful
characteristics:

* the probability of falsely reporting a node as failed is known

* the algorithm is resilient against network loss and it will recover
  from network partitioning

* detection time scales with the number of nodes as *O(n log n)*

The implementation uses Go's own ``net/rpc`` RPC module.

