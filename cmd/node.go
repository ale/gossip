// An example implementation of a gossip-based service.
//
package main

import (
	"flag"
	"log"
	"net/http"
	"net/rpc"

	_ "net/http/pprof"

	"git.autistici.org/ale/gossip"
)

var (
	nodeName = flag.String("name", "", "Name")
	addr     = flag.String("addr", "", "TCP addr")
)

// Our application-specific data is just the node name.
type NodeData struct {
	Name string
}

func main() {
	flag.Parse()

	if *nodeName == "" || *addr == "" {
		log.Fatalf("Must specify --name and --addr")
	}

	data := &NodeData{*nodeName}
	g := gossip.NewGossip(*addr, data, flag.Args())
	go g.Run()

	rpc.HandleHTTP()
	log.Println(http.ListenAndServe(*addr, nil))
}
