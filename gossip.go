// A gossip-based discovery service.
//
// This package is based on some of the ideas found in the paper
// "A Gossip-Style Failure Detection Service" by van Renesse,
// Minsky, Hayden[1].
//
// [1] http://www.cs.cornell.edu/home/rvr/papers/GossipFD.pdf
//
// (c) 2014 <ale@incal.net>. See the file COPYING for details.
//
package gossip

import (
	"encoding/gob"
	"fmt"
	"log"
	"math/rand"
	"net/rpc"
	"sync"
	"time"

	"git.autistici.org/ale/rrpc"
)

var (
	// How many propagation constants one node must miss in order
	// for it to be considered dead (scales with the observed
	// cluster size).
	Nfailures = 3

	// How often to broadcast to other nodes (default).
	Tgossip = 1 * time.Second

	// RPC deadline (default).
	Trpc = 5 * time.Second
)

// Member represents a participant in the gossip protocol.
// Applications can provide opaque data, which will be broadcasted to
// all the other participants.
type Member struct {
	// Address of this node (host:port).
	Addr string

	// Heartbeat counter.
	Heartbeat int64

	// Application-specific data.
	Data interface{}
}

func (m *Member) String() string {
	return fmt.Sprintf("%s(%d)", m.Addr, m.Heartbeat)
}

// This interface makes it possible to stub out the RPC api for
// testing purposes.
type rpcApi interface {
	Call(addr, method string, req, resp interface{}, opts *rrpc.Options) error
}

type rrpcInterface struct {
	clients map[string]*rrpc.Client
}

func newRrpcInterface() *rrpcInterface {
	return &rrpcInterface{
		clients: make(map[string]*rrpc.Client),
	}
}

// Return the RPC client for addr, possibly from cache.
func (r *rrpcInterface) client(addr string) *rrpc.Client {
	if client, ok := r.clients[addr]; ok {
		return client
	}
	client := rrpc.New(addr, nil)
	r.clients[addr] = client
	return client
}

func (r *rrpcInterface) Call(addr, method string, req, resp interface{}, opts *rrpc.Options) error {
	return r.client(addr).Call(method, req, resp, opts)
}

// Gossip represents a participant in the protocol. It contains the
// full connection state and membership information.
//
// Example:
//
//        g = New(...)
//        go g.Run()
//        for {
//                <-g.C
//                // do something with the member list
//        }
//
// A separate goroutine should periodically call gossip.SetData() to
// update the application-specific local state which is shared with
// the other peers.
//
type Gossip struct {
	// List of members. The relation members[0] == self is always
	// preserved (they point at the same object).
	self    *Member
	members map[string]*Member

	// Separate pool of dead nodes, optimized for fast membership
	// checks and selection of a random element.
	dead *uniqueStringList

	// Freshness of the various nodes, by address.
	lastHeartbeat map[string]time.Time

	rpc  rpcApi
	lock sync.Mutex
	quit chan bool

	// Updates channel.
	C chan bool

	// Set to true to enable debug output.
	DebugOutput bool

	// Timeouts.
	Tgossip time.Duration
	Trpc    time.Duration
}

func New(addr string, data interface{}, otherNodes []string) *Gossip {
	// Make sure that 'gob' knows about the application-specific
	// data type, since we're casting to interface{} on the rpc
	// transport.
	gob.Register(data)

	self := &Member{Addr: addr, Data: data}

	g := &Gossip{
		self: self,
		members: map[string]*Member{
			addr: self,
		},
		lastHeartbeat: make(map[string]time.Time),
		dead:          newUniqueStringList(),
		rpc:           newRrpcInterface(),
		C:             make(chan bool, 1),
		Tgossip:       Tgossip,
		Trpc:          Trpc,
		quit:          make(chan bool),
	}
	// Load all the bootstrap addresses as non-active members.
	for _, addr := range otherNodes {
		g.dead.Add(addr)
	}

	g.serveRpc()

	return g
}

func (g *Gossip) debug(format string, args ...interface{}) {
	if g.DebugOutput {
		log.Printf("%s: %s", g.self.Addr, fmt.Sprintf(format, args...))
	}
}

func (g *Gossip) Close() {
	close(g.quit)
	c := g.C
	g.C = nil
	close(c)
}

// SetData updates the application-specific data for this node.
func (g *Gossip) SetData(data interface{}) {
	g.lock.Lock()
	defer g.lock.Unlock()
	g.self.Data = data
}

// FailureTime returns the current time after which a node is
// considered dead. It is based on the gossip tick interval and the
// observed cluster size.
func (g *Gossip) FailureTime() time.Duration {
	return time.Duration(Nfailures*len(g.members)) * g.Tgossip
}

// Return the list of active members.
func (g *Gossip) GetMembers() []Member {
	g.lock.Lock()
	defer g.lock.Unlock()
	return g.getMembers()
}

// Return the list of active members (without acquiring the mutex).
func (g *Gossip) getMembers() []Member {
	now := time.Now()

	out := make([]Member, 0, len(g.members))
	for _, m := range g.members {
		// Skip non alive members. Self will always be
		// included because it never appears in the
		// lastHeartbeat map, but let's have an explicit check
		// anyway.
		if lastHb, ok := g.lastHeartbeat[m.Addr]; ok && !g.isSelf(*m) && !g.isAlive(now, lastHb) {
			continue
		}
		out = append(out, *m)
	}
	return out
}

// Receive a remote update.
func (g *Gossip) Receive(update []Member) {
	g.lock.Lock()
	defer g.lock.Unlock()

	now := time.Now()

	// Merge the incoming list with the one we already have: keep
	// the most recent partial data (member-wise) from each. Use a
	// temporary map to merge the members using their address as
	// key.
	for _, m := range update {
		// Ignore other members' ideas about ourselves (as we
		// always have the freshest copy of 'self').
		if g.isSelf(m) {
			continue
		}
		if cur, ok := g.members[m.Addr]; ok {
			// Update this (already known) node only if
			// the heartbeat is more recent than what we
			// have stored (to preserve the freshest
			// version of Data).
			if cur.Heartbeat < m.Heartbeat {
				g.lastHeartbeat[m.Addr] = now
				cur.Heartbeat = m.Heartbeat
				cur.Data = m.Data
			}
		} else {
			g.debug("discovered new node: %s", m.Addr)

			// Add the new member to our member list. We
			// make a fresh copy to isolate ourselves from
			// changes to 'update' after this method
			// returns.
			g.members[m.Addr] = &Member{
				Addr:      m.Addr,
				Heartbeat: m.Heartbeat,
				Data:      m.Data,
			}
			g.lastHeartbeat[m.Addr] = now

			// Remove from the list of dead members (in
			// case it's a reconnection).
			g.dead.Discard(m.Addr)
		}
	}

	// Non-blocking send to the wakeup channel.
	if g.C != nil {
		select {
		case g.C <- true:
		default:
		}
	}
}

// Select a random target for the next packet.
func (g *Gossip) pickGossipTarget(members []Member) string {
	// If there are dead nodes, pick one of them with probability
	// 1/(n+1) with n number of active nodes (the +1 is just there
	// to ensure reasonable cluster operations with very few
	// nodes). This ensures that, on average, a stream of 1
	// broadcast per tick from the cluster will flow towards
	// unavailable nodes, preventing long-term partitioning of the
	// network.
	if g.dead.Len() > 0 && rand.Intn(len(g.members)+1) == 0 {
		return g.dead.At(rand.Intn(g.dead.Len()))
	}

	// Otherwise pick a random element from the list of currently
	// active members, excluding self.
	tmp := make([]string, 0, len(members))
	for _, m := range members {
		if !g.isSelf(m) {
			tmp = append(tmp, m.Addr)
		}
	}
	if len(tmp) < 1 {
		return ""
	}
	return tmp[rand.Intn(len(tmp))]
}

// Run the main gossip broadcast loop.
func (g *Gossip) runBroadcast() {
	t := time.NewTicker(g.Tgossip)
	for {
		select {
		case <-t.C:
			g.lock.Lock()

			// Increment our heartbeat. This should be a
			// monotonically increasing counter, bonus
			// points if we don't have to store it between
			// restarts - so we use the number of
			// milliseconds elapsed since the epoch.
			g.self.Heartbeat = time.Now().UnixNano() / 1000000

			members := g.getMembers()
			addr := g.pickGossipTarget(members)
			g.lock.Unlock()

			if addr == "" {
				g.debug("no live peers (members=%+v, GetMembers=%+v)", g.members, members)
			} else {
				// Run the ping in a separate function so that
				// we can use a timeout that is longer than
				// Tgossip.
				req := GossipRequest{members}
				var resp GossipResponse
				go func() {
					g.debug("ping %s  %v", addr, req.Members)
					if err := g.rpc.Call(addr, "Gossip.Receive", &req, &resp, &rrpc.Options{Deadline: Trpc}); err != nil {
						g.debug("rpc error (target=%s): %v", addr, err)
					}
				}()
			}
		case <-g.quit:
			return
		}
	}
}

// Expire inactive members after failure time has passed without any
// node reporting them as active.
func (g *Gossip) expire() {
	g.lock.Lock()
	defer g.lock.Unlock()

	now := time.Now()

	for _, m := range g.members {
		// Do not clean up self.
		if g.isSelf(*m) {
			continue
		}
		// Skip nodes that are already marked dead.
		if g.dead.Has(m.Addr) {
			continue
		}
		if lastHb, ok := g.lastHeartbeat[m.Addr]; ok && !g.isAlive(now, lastHb) {
			g.debug("node %s is dead", m.Addr)
			g.dead.Add(m.Addr)
		}
	}
}

// Call expire() periodically.
func (g *Gossip) runExpire() {
	t := time.NewTicker(5 * g.Tgossip)
	for {
		select {
		case <-t.C:
			g.expire()
		case <-g.quit:
			return
		}
	}
}

func (g *Gossip) runDebug() {
	t := time.NewTicker(10 * time.Second)
	for {
		<-t.C
		g.debug("%s: gossip state = %v", g.self.Addr, g.GetMembers())
	}
}

// Run starts the node (does not return).
func (g *Gossip) Run() {
	go g.runExpire()
	g.runBroadcast()
}

type GossipRequest struct {
	Members []Member
}

type GossipResponse struct{}

type GetMembersRequest struct{}

type GetMembersResponse struct {
	Members []Member
}

type gossipRpc struct {
	gossip *Gossip
}

func (g *gossipRpc) GetMembers(req *GetMembersRequest, resp *GetMembersResponse) error {
	resp.Members = g.gossip.GetMembers()
	return nil
}

func (g *gossipRpc) Receive(req *GossipRequest, resp *GossipResponse) error {
	g.gossip.Receive(req.Members)
	return nil
}

func (g *Gossip) serveRpc() {
	rpc.RegisterName("Gossip", &gossipRpc{g})
}

func (g *Gossip) isAlive(now, lastHeartbeat time.Time) bool {
	return now.Sub(lastHeartbeat) < g.FailureTime()
}

func (g *Gossip) isSelf(m Member) bool {
	return m.Addr == g.self.Addr
}
