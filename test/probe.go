// Collect and print statistics on a running cluster.
//
package main

import (
	"encoding/gob"
	"flag"
	"fmt"
	"log"
	"net/rpc"
	"sort"
	"strings"
	"time"

	"git.autistici.org/ale/gossip"
)

type nodeStats struct {
	members []*gossip.Member
}

func getStats(node string) *nodeStats {
	client, err := rpc.DialHTTP("tcp", node)
	if err != nil {
		log.Println(err)
		return nil
	}
	defer client.Close()

	var req gossip.GetMembersRequest
	var resp gossip.GetMembersResponse
	if err := client.Call("Gossip.GetMembers", &req, &resp); err != nil {
		log.Println(err)
		return nil
	}

	return &nodeStats{resp.Members}
}

type heartbeatStats struct {
	min, max int64
}

func (s *heartbeatStats) String() string {
	return fmt.Sprintf("(%d: %d-%d)", (s.max - s.min), s.min, s.max)
}

type memberList []*gossip.Member

type sortedMemberList struct {
	memberList
}

func (ml memberList) Len() int           { return len(ml) }
func (ml memberList) Swap(i, j int)      { ml[i], ml[j] = ml[j], ml[i] }
func (ml memberList) Less(i, j int) bool { return ml[i].Addr < ml[j].Addr }

func membersToString(members []*gossip.Member) string {
	ml := sortedMemberList{members}
	sort.Sort(ml)
	addrs := make([]string, 0)
	for _, m := range members {
		addrs = append(addrs, m.Addr)
	}
	return strings.Join(addrs, ",")
}

func dumpStats(nodes []string) {
	okNodes := make([]string, 0)
	stats := make(map[string]*nodeStats)
	for _, node := range nodes {
		s := getStats(node)
		if s == nil {
			continue
		}
		stats[node] = s
		okNodes = append(okNodes, node)
	}

	if len(okNodes) == 0 {
		return
	}

	// Check if all members agree on the view of nodes.
	// To do this turn the first set of members into a sorted
	// list and compare the other ones.
	matching := true
	ref := membersToString(stats[okNodes[0]].members)
	for _, node := range okNodes[1:] {
		s := membersToString(stats[node].members)
		if s != ref {
			matching = false
			break
		}
	}

	// Compute per-target heartbeat status.
	hbstats := make(map[string]*heartbeatStats)
	for _, node := range okNodes {
		for _, member := range stats[node].members {
			hbvalue := member.Heartbeat
			stats, ok := hbstats[member.Addr]
			if !ok {
				stats = &heartbeatStats{hbvalue, hbvalue}
				hbstats[member.Addr] = stats
			}
			if hbvalue < stats.min {
				stats.min = hbvalue
			}
			if hbvalue > stats.max {
				stats.max = hbvalue
			}
		}
	}

	log.Printf("matching=%v, hbstats=%v", matching, hbstats)
}

// Our application-specific data is just the node name.
// We have to trick 'gob' into thinking this is the same object
// as in cmd/node.go.
type NodeData struct {
	Name string
}

func main() {
	gob.Register(&NodeData{})

	flag.Parse()
	nodes := flag.Args()

	for _ = range time.NewTicker(3 * time.Second).C {
		dumpStats(nodes)
	}
}
