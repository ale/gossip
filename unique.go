package gossip

// uniqueStringList is a list of unique strings, with fast membership
// check and direct addressing (so that we can quickly select a random
// element).
type uniqueStringList struct {
	l   []string
	all map[string]struct{}
}

func newUniqueStringList() *uniqueStringList {
	return &uniqueStringList{
		all: make(map[string]struct{}),
	}
}

func (u *uniqueStringList) Add(s string) {
	if !u.Has(s) {
		u.l = append(u.l, s)
		u.all[s] = struct{}{}
	}
}

func (u *uniqueStringList) Has(s string) bool {
	_, ok := u.all[s]
	return ok
}

func (u *uniqueStringList) Len() int {
	return len(u.l)
}

func (u *uniqueStringList) At(i int) string {
	return u.l[i]
}

func (u *uniqueStringList) Discard(s string) {
	if !u.Has(s) {
		return
	}
	delete(u.all, s)
	out := make([]string, 0, len(s)-1)
	for _, ss := range u.l {
		if ss != s {
			out = append(out, ss)
		}
	}
	u.l = out
}
